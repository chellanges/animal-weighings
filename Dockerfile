FROM python:3.7.4-slim-buster
LABEL maintainer="Fernando Júnior <junio.webmaster@gmail.com>"

ENV PYTHONUNBUFFERED 1
ENV TERM xterm

COPY ./Pipfile /Pipfile
COPY ./Pipfile.lock Pipfile.lock

ENV BUILD_DEPS="build-essential" \
    APP_DEPS="curl libpq-dev"

RUN apt-get update \
  && apt-get install -y ${BUILD_DEPS} ${APP_DEPS} --no-install-recommends \
  && pip install pipenv \
  && pipenv install --dev --deploy --system \
  && rm -rf /var/lib/apt/lists/* \
  && rm -rf /usr/share/doc && rm -rf /usr/share/man \
  && apt-get purge -y --auto-remove ${BUILD_DEPS} \
  && apt-get clean

COPY . /

WORKDIR /animal_weighings

EXPOSE 8000

CMD ["gunicorn", "animal_weighings.wsgi", "0:8000"]
