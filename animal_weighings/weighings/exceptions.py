from rest_framework.exceptions import ValidationError


class NotConsistentWeight(ValidationError):
    status_code = 400
    default_detail = 'Weight value is not consistent'
    default_code = 'not_consistent_weight'
