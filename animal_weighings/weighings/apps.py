from django.apps import AppConfig


class WeighingsConfig(AppConfig):
    name = 'weighings'

    def ready(self):
        # noinspection PyUnresolvedReferences
        import weighings.signals  # noqa
