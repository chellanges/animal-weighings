import datetime
import pytz

from decimal import Decimal
from unittest import mock

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase

from cattles.models import Cattle
from ranches.models import Ranch
from weighings.models import Weighing


class WeighingFunctionalTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        new_user = MyUser.objects.create_user(email='user@email.com')
        new_ranch = Ranch.objects.create(name='Test Ranch', user=new_user)
        self.new_cattle = Cattle.objects.create(type='cow',
                                                ear_tag='sjhe645bk!@?kuw4678!',
                                                ranch=new_ranch)
        self.mocked = datetime.datetime(2019, 9, 19, 0, 0, 0, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=self.mocked)):
            self.new_weighing = Weighing.objects.create(weight=27.578,
                                                        cattle=self.new_cattle)

    def test_new_weighing(self):
        """
        GIVEN a Weighing model
        WHEN a new Weighing is created
        THEN check the date_time, weight and cattle are defined correctly
        """
        self.assertEqual(self.new_weighing.date_time, self.mocked)
        self.assertEqual(self.new_weighing.weight, 27.578)
        self.assertEqual(self.new_weighing.cattle.ear_tag, 'sjhe645bk!@?kuw4678!')
        self.assertIsInstance(self.new_weighing.cattle, Cattle)

    def test_weighing_exists_in_database(self):
        """
        GIVEN a Weighing model
        WHEN a new Weighing is created
        THEN check if exists in database
        """
        existing_weighing = Weighing.objects.filter(weight=27.578,
                                                    cattle_id=self.new_cattle.id)
        self.assertTrue(existing_weighing.exists())

    def test_previous_weight(self):
        """
        GIVEN a Weighing model
        WHEN a new Weighing is created
        THEN check the previous_weight is defined correctly
        """
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=self.mocked)):
            other_weighing = Weighing.objects.create(weight=38.312,
                                                     cattle=self.new_cattle)
            self.assertEqual(other_weighing.previous_weight, Decimal('27.578'))

    def test_previous_weight_is_none(self):
        """
        GIVEN a Weighing model
        WHEN a first Weighing is created
        THEN check the previous_weight is None
        """
        self.assertIsNone(self.new_weighing.previous_weight)

    def test_weight_gain(self):
        """
        GIVEN a Weighing model
        WHEN a new Weighing is created
        THEN check the weight_gain is defined correctly
        """
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=self.mocked)):
            other_weighing = Weighing.objects.create(weight=38.312,
                                                     cattle=self.new_cattle)
            self.assertEqual(other_weighing.weight_gain, Decimal('10.734'))

    def test_weight_gain_is_none(self):
        """
        GIVEN a Weighing model
        WHEN a first Weighing is created
        THEN check the weight_gain is None
        """
        self.assertIsNone(self.new_weighing.weight_gain)

    def test_validate_weight(self):
        """
        GIVEN a Weighing model
        WHEN a new Weighing is created
        THEN check if weight value is consistent
        """
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=self.mocked)):
            with self.assertRaises(ValidationError):
                Weighing.objects.create(weight=0, cattle=self.new_cattle)
            with self.assertRaises(ValidationError):
                Weighing.objects.create(weight=57.578, cattle=self.new_cattle)
