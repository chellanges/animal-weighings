from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase

from cattles.models import Cattle
from ranches.models import Ranch

from weighings.tasks import add_weighing


class TasksFunctionalTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        new_user = MyUser.objects.create_user(email='user@email.com')
        new_ranch = Ranch.objects.create(name='Test Ranch', user=new_user)
        self.new_cattle = Cattle.objects.create(
            type='cow', ear_tag='sjhe645bk!@?kuw4678!', ranch=new_ranch)

    def tests_task_add_valid_weighing(self):
        """
        GIVEN a add_weighing task
        WHEN a task add_weighing is created
        THEN check the date_time, weight and cattle are defined correctly
        """
        valid_payload = {
            'weight': 27.578,
            'cattle__id': 1,
        }
        self.new_weighing = add_weighing(valid_payload)
        self.assertEqual(self.new_weighing['weight'], '27.578')
        self.assertEqual(self.new_weighing['cattle__id'], 1)
        self.assertEqual(self.new_weighing['cattle__ear_tag'], 'sjhe645bk!@?kuw4678!')

    def tests_task_add_invalid_weighing(self):
        """
        GIVEN a add_weighing task
        WHEN a task add_weighing is created
        THEN check if weight value is consistent
        """
        valid_payload = {
            'weight': 27.578,
            'cattle__id': 1,
        }
        invalid_first_payload = {
            'weight': 0,
            'cattle__id': 1,
        }
        invalid_second_payload = {
            'weight': 57.578,
            'cattle__id': 1,
        }
        self.new_weighing = add_weighing(valid_payload)

        with self.assertRaises(ValidationError):
            self.new_weighing = add_weighing(invalid_first_payload)
        with self.assertRaises(ValidationError):
            self.new_weighing = add_weighing(invalid_second_payload)
