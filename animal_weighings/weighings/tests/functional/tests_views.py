import json

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status

from cattles.models import Cattle
from ranches.models import Ranch
from weighings.models import Weighing
from weighings.serializers import WeighingSerializer


class GetAllWeighingTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)
        new_cattle = Cattle.objects.create(type='cow',
                                           ear_tag='sjhe645bk!@?kuw4678!',
                                           ranch=new_ranch)

        Weighing.objects.create(weight=27.578, cattle=new_cattle)
        Weighing.objects.create(weight=35.878, cattle=new_cattle)
        Weighing.objects.create(weight=42.508, cattle=new_cattle)
        Weighing.objects.create(weight=53.986, cattle=new_cattle)

    def test_get_all_weighing(self):
        """
        GIVEN a Django application
        WHEN the '/api/v1/weighings' page is requested (GET)
        THEN check the response is valid
        """
        # get API response
        response = self.client.get(reverse('weighings-list'))
        # get data from db
        weighings = Weighing.objects.all()
        serializer = WeighingSerializer(weighings, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleWeighingTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)
        new_cattle = Cattle.objects.create(type='cow',
                                           ear_tag='sjhe645bk!@?kuw4678!',
                                           ranch=new_ranch)

        self.weighing_first = Weighing.objects.create(weight=27.578, cattle=new_cattle)
        self.weighing_second = Weighing.objects.create(weight=35.878, cattle=new_cattle)
        self.weighing_third = Weighing.objects.create(weight=42.508, cattle=new_cattle)
        self.weighing_fourth = Weighing.objects.create(weight=53.986, cattle=new_cattle)

    def test_get_valid_single_weighing(self):
        response = self.client.get(
            reverse('weighings-detail', kwargs={'pk': self.weighing_first.pk}))
        weighing = Weighing.objects.get(pk=self.weighing_first.pk)
        serializer = WeighingSerializer(weighing)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_weighing(self):
        response = self.client.get(
            reverse('weighings-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UpdateSingleWeighingTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)
        new_cattle = Cattle.objects.create(type='cow',
                                           ear_tag='sjhe645bk!@?kuw4678!',
                                           ranch=new_ranch)
        self.weighing_first = Weighing.objects.create(weight=27.578, cattle=new_cattle)
        self.weighing_second = Weighing.objects.create(weight=35.878, cattle=new_cattle)
        self.valid_payload = {
            'weight': 38.986,
            'cattle__id': 1,
        }
        self.invalid_payload = {
            'weight': 47.116,
            'cattle__id': 19,
        }

    def test_valid_update_weighing(self):
        response = self.client.put(
            reverse('weighings-detail', kwargs={'pk': self.weighing_first.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_weighing(self):
        response = self.client.put(
            reverse('weighings-detail', kwargs={'pk': self.weighing_second.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleWeighingTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)
        new_cattle = Cattle.objects.create(type='cow',
                                           ear_tag='sjhe645bk!@?kuw4678!',
                                           ranch=new_ranch)
        self.weighing_first = Weighing.objects.create(weight=27.578, cattle=new_cattle)
        self.weighing_second = Weighing.objects.create(weight=35.878, cattle=new_cattle)

    def test_valid_delete_weighing(self):
        response = self.client.delete(
            reverse('weighings-detail', kwargs={'pk': self.weighing_first.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_weighing(self):
        response = self.client.delete(
            reverse('weighings-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
