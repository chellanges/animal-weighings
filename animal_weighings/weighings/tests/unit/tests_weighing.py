from django.contrib.auth import get_user_model
from django.test import TestCase

from cattles.models import Cattle
from ranches.models import Ranch
from weighings.models import Weighing


class WeighingUnitTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        new_user = MyUser(email='test@email.com')
        new_ranch = Ranch(name='Test Ranch', user=new_user)
        new_cattle = Cattle(type='cow',
                            ear_tag='sjhe645bk!@?kuw4678!',
                            ranch=new_ranch)
        self.new_weighing = Weighing(date_time='2019-09-19 13:45:27',
                                     weight=27.578,
                                     cattle=new_cattle)

    def test_new_weighing(self):
        """
        GIVEN a Weighing model
        WHEN a new Weighing is created
        THEN check the date_time, weight and cattle are defined correctly
        """
        self.assertEqual(self.new_weighing.date_time, '2019-09-19 13:45:27')
        self.assertEqual(self.new_weighing.weight, 27.578)
        self.assertEqual(self.new_weighing.cattle.type, 'cow')
        self.assertEqual(self.new_weighing.cattle.ear_tag, 'sjhe645bk!@?kuw4678!')
        self.assertIsInstance(self.new_weighing.cattle, Cattle)
