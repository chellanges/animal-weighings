from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Weighing
from .serializers import WeighingSerializer
from .tasks import add_weighing


class WeighingViewSet(viewsets.ModelViewSet):
    queryset = Weighing.objects.all()
    serializer_class = WeighingSerializer

    def create(self, request, *args, **kwargs):
        task = add_weighing.delay(request.data)
        headers = {'Location': reverse(
            'tasks-detail',
            request=request,
            kwargs={'task_id': task.id}
        )}
        return Response({}, status=status.HTTP_202_ACCEPTED, headers=headers)
