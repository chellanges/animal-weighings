from django_filters import rest_framework as filters
from django_filters import DateFilter

from .models import Weighing


class WeighingsFilter(filters.FilterSet):
    start_date = DateFilter(field_name='date_time', lookup_expr='gt', )
    end_date = DateFilter(field_name='date_time', lookup_expr='lt', )

    class Meta:
        model = Weighing
        fields = ['cattle__id', 'cattle__ear_tag', 'cattle__ranch__id', ]
