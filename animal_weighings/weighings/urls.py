from django.urls import include, path
from rest_framework import routers

from .views import WeighingViewSet

router = routers.DefaultRouter()
router.register(r'', WeighingViewSet, basename='weighings')

urlpatterns = [
    path('', include(router.urls)),
]
