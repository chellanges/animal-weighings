from decimal import Decimal

from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import Weighing


@receiver(pre_save, sender=Weighing)
def validate_weight(sender, **kwargs):
    instance = kwargs['instance']
    if instance.weight <= 0:
        raise ValidationError('Weight value is not consistent', code='not_consistent_weight')
    elif instance.weight_gain is not None:
        percentage = (Decimal(instance.weight_gain) / Decimal(instance.previous_weight)) * 100
        if percentage > 80:
            raise ValidationError('Weight value is not consistent', code='not_consistent_weight')
