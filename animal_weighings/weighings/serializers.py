from rest_framework import serializers

from cattles.models import Cattle
from .models import Weighing


class WeighingSerializer(serializers.ModelSerializer):
    cattle__id = serializers.PrimaryKeyRelatedField(
        source='cattle', queryset=Cattle.objects.all())
    cattle__ear_tag = serializers.ReadOnlyField(source='cattle.ear_tag')

    class Meta:
        model = Weighing
        fields = ['date_time', 'weight', 'weight_gain', 'cattle__id', 'cattle__ear_tag']
