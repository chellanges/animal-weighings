from animal_weighings.celery import app

from .serializers import WeighingSerializer


@app.task
def add_weighing(data):
    serializer = WeighingSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return serializer.data
