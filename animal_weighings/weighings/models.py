from decimal import Decimal
from django.db import models

from cattles.models import Cattle


class Weighing(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)
    weight = models.DecimalField(max_digits=5, decimal_places=3)
    cattle = models.ForeignKey(
        Cattle,
        related_name='weighings',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return '({}) {}'.format(self.date_time.strftime('%d/%m/%Y'), self.weight)

    @property
    def previous_weight(self):
        previous_weight = Weighing.objects.select_related('cattle').values_list(
            'weight',
            flat=True,
        ).filter(
            cattle_id=self.cattle.id,
            weight__lt=self.weight,
        ).order_by(
            'weight',
        ).last()

        return previous_weight

    @property
    def weight_gain(self):
        if self.previous_weight is not None:
            gain = Decimal(self.weight) - Decimal(self.previous_weight)
            return round(gain, 3)
