from django.apps import AppConfig


class RanchesConfig(AppConfig):
    name = 'ranches'
