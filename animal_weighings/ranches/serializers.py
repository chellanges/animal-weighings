from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Ranch


class RanchSerializer(serializers.ModelSerializer):
    MyUser = get_user_model()
    user__id = serializers.PrimaryKeyRelatedField(
        source='user', queryset=MyUser.objects.all())
    user__email = serializers.ReadOnlyField(source='user.email')

    class Meta:
        model = Ranch
        fields = ['name', 'user__id', 'user__email']
