from django.contrib.auth import get_user_model
from django.db import models


class Ranch(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(
        get_user_model(),
        related_name='ranches',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return '({}) {}'.format(self.user, self.name)
