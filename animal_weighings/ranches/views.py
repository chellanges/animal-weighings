from rest_framework import viewsets

from .models import Ranch
from .serializers import RanchSerializer


class RanchViewSet(viewsets.ModelViewSet):
    queryset = Ranch.objects.all()
    serializer_class = RanchSerializer
