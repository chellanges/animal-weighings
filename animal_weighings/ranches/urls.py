from django.urls import include, path
from rest_framework import routers

from .views import RanchViewSet

router = routers.DefaultRouter()
router.register(r'', RanchViewSet, basename='ranches')

urlpatterns = [
    path('', include(router.urls)),
]
