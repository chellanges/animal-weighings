from django.db import IntegrityError
from django.contrib.auth import get_user_model
from django.test import TestCase

from ranches.models import Ranch


class RanchFunctionalTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        new_user = MyUser.objects.create_user(email='user@email.com')
        self.new_ranch = Ranch.objects.create(name='Test Ranch', user=new_user)

    def test_new_ranch(self):
        """
        GIVEN a Ranch model
        WHEN a new Ranch is created
        THEN check the values are defined correctly
        """
        self.assertEqual(self.new_ranch.name, 'Test Ranch')
        self.assertEqual(self.new_ranch.user.email, 'user@email.com')
        self.assertIsInstance(self.new_ranch.user, get_user_model())

    def test_ranch_exists_in_database(self):
        """
        GIVEN a Ranch model
        WHEN a new Ranch is created
        THEN check if exists in database
        """
        existing_ranch = Ranch.objects.filter(name='Test Ranch')
        self.assertTrue(existing_ranch.exists())

    def test_not_delete_user_with_ranch(self):
        """
        GIVEN a User model
        WHEN a User is deleted
        THEN check exception thrown when delete user with ranch
        """
        MyUser = get_user_model()
        user = MyUser.objects.get(email='user@email.com')
        with self.assertRaises(IntegrityError):
            user.delete()
