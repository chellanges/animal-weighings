import json

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status

from ranches.models import Ranch
from ranches.serializers import RanchSerializer


class GetAllRanchesTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        Ranch.objects.create(name='Test Ranch first', user=user)
        Ranch.objects.create(name='Test Ranch second', user=user)
        Ranch.objects.create(name='Test Ranch third', user=user)
        Ranch.objects.create(name='Test Ranch fourth', user=user)

    def test_get_all_ranches(self):
        """
        GIVEN a Django application
        WHEN the '/api/v1/ranches' page is requested (GET)
        THEN check the response is valid
        """
        # get API response
        response = self.client.get(reverse('ranches-list'))
        # get data from db
        ranches = Ranch.objects.all()
        serializer = RanchSerializer(ranches, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleRanchesTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        self.ranch_first = Ranch.objects.create(name='Test Ranch first', user=user)
        self.ranch_second = Ranch.objects.create(name='Test Ranch second', user=user)
        self.ranch_third = Ranch.objects.create(name='Test Ranch third', user=user)
        self.ranch_fourth = Ranch.objects.create(name='Test Ranch fourth', user=user)

    def test_get_valid_single_ranch(self):
        response = self.client.get(
            reverse('ranches-detail', kwargs={'pk': self.ranch_first.pk}))
        ranch = Ranch.objects.get(pk=self.ranch_first.pk)
        serializer = RanchSerializer(ranch)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_ranch(self):
        response = self.client.get(
            reverse('ranches-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewRanchTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        self.valid_payload = {
            'name': 'Test Ranch first',
            'user__id': 1,
        }

        self.invalid_payload = {
            'name': 'Test Ranch second',
            'user__id': 5,
        }

    def test_create_valid_ranch(self):
        response = self.client.post(
            reverse('ranches-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_ranch(self):
        response = self.client.post(
            reverse('ranches-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleRanchTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        self.ranch_first = Ranch.objects.create(
            name='Test Ranch first', user=user)
        self.ranch_second = Ranch.objects.create(
            name='Test Ranch second', user=user)
        self.valid_payload = {
            'name': 'Test Ranch first (new)',
            'user__id': 1,
        }
        self.invalid_payload = {
            'name': 'Test Ranch second (new)',
            'user__id': 5,
        }

    def test_valid_update_ranch(self):
        response = self.client.put(
            reverse('ranches-detail', kwargs={'pk': self.ranch_first.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_ranch(self):
        response = self.client.put(
            reverse('ranches-detail', kwargs={'pk': self.ranch_second.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleRanchTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        self.ranch_first = Ranch.objects.create(
            name='Test Ranch first', user=user)
        self.ranch_second = Ranch.objects.create(
            name='Test Ranch second', user=user)

    def test_valid_delete_ranch(self):
        response = self.client.delete(
            reverse('ranches-detail', kwargs={'pk': self.ranch_first.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_ranch(self):
        response = self.client.delete(
            reverse('ranches-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
