from django.contrib.auth import get_user_model
from django.test import TestCase

from ranches.models import Ranch


class RanchUnitTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        new_user = MyUser(email='test@email.com')
        self.new_ranch = Ranch(name='Test Ranch', user=new_user)

    def test_new_ranch(self):
        """
        GIVEN a Ranch model
        WHEN a new Ranch is created
        THEN check the name and user are defined correctly
        """
        self.assertEqual(self.new_ranch.name, 'Test Ranch')
        self.assertEqual(self.new_ranch.user.email, 'test@email.com')
        self.assertIsInstance(self.new_ranch.user, get_user_model())
