from django.contrib.auth.models import BaseUserManager


class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=self.normalize_email(email))

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        if not email:
            raise ValueError('Admins must have an email address')
        if not password:
            raise ValueError('Admins must have a password')

        user = self.create_user(self.normalize_email(email), password=password)
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user
