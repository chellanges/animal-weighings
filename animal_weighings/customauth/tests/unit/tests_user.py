from django.contrib.auth import get_user_model
from django.test import TestCase


class UserUnitTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        self.new_user = MyUser(email='test@email.com')

    def test_new_user(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser is created
        THEN check the email are defined correctly
        """
        self.assertEqual(self.new_user.email, 'test@email.com')

    def test_new_user_default_values(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser is created
        THEN check the default values are defined correctly
        """
        self.assertTrue(self.new_user.is_active)
        self.assertFalse(self.new_user.is_admin)
        self.assertFalse(self.new_user.is_staff)
