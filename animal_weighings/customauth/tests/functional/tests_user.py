from django.contrib.auth import get_user_model
from django.test import TestCase


class UserFunctionalTestCase(TestCase):
    def setUp(self):
        User = get_user_model()
        self.new_user = User.objects.create_user(email='user@email.com', password='foo')
        self.new_admin = User.objects.create_superuser(email='super@email.com', password='foo')

    def test_new_user(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser is created
        THEN check the values are defined correctly
        """
        self.assertEqual(self.new_user.email, 'user@email.com')
        self.assertTrue(self.new_user.is_active)
        self.assertFalse(self.new_user.is_staff)
        self.assertFalse(self.new_user.is_admin)

    def test_new_admin(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser as admin is created
        THEN check the values are defined correctly
        """
        self.assertEqual(self.new_admin.email, 'super@email.com')
        self.assertTrue(self.new_admin.is_active)
        self.assertTrue(self.new_admin.is_staff)
        self.assertTrue(self.new_admin.is_admin)

    def test_users_exists_in_database(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser is created
        THEN check if exists in database
        """
        User = get_user_model()
        existing_user = User.objects.filter(email=self.new_user.email)
        existing_admin = User.objects.filter(email=self.new_admin.email)
        self.assertTrue(existing_user.exists())
        self.assertTrue(existing_admin.exists())

    def test_new_user_without_password(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser without password is created
        THEN check the values are defined correctly
        """
        User = get_user_model()
        new_user = User.objects.create_user(email='user_without_pass@email.com')
        self.assertEqual(new_user.email, 'user_without_pass@email.com')
        self.assertTrue(new_user.is_active)
        self.assertFalse(new_user.is_staff)
        self.assertFalse(new_user.is_admin)

    def test_new_user_when_email_is_empty(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser is created
        THEN check exception thrown when email is empty
        """
        User = get_user_model()
        with self.assertRaises(ValueError):
            User.objects.create_user(email='')

    def test_new_admin_when_email_and_or_password_is_empty(self):
        """
        GIVEN a MyUser model
        WHEN a new MyUser as admin is created
        THEN check exception thrown when email and/or password is empty
        """
        User = get_user_model()
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='', password='')
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='', password='foo')
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='super@email.com', password='')
