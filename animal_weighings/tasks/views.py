from rest_framework import viewsets

from django_celery_results.models import TaskResult

from .serializers import TaskResultSerializer


class TaskResultViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = TaskResultSerializer
    queryset = TaskResult.objects.all()
    lookup_field = 'task_id'
