from django.urls import include, path
from rest_framework import routers

from .views import TaskResultViewSet

router = routers.DefaultRouter()
router.register(r'', TaskResultViewSet, basename='tasks')

urlpatterns = [
    path('', include(router.urls)),
]
