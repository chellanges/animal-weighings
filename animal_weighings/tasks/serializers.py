from rest_framework import serializers

from django_celery_results.models import TaskResult


class TaskResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskResult
        lookup_field = 'task_id'
        fields = '__all__'
