from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework.views import exception_handler

from cattles.exceptions import DuplicatedCattle
from weighings.exceptions import NotConsistentWeight


def custom_exception_handler(exc, context):
    if isinstance(exc, DjangoValidationError):
        if hasattr(exc, 'message') and hasattr(exc, 'code'):
            if exc.code == 'duplicated_cattle':
                exc = DuplicatedCattle(detail={'error': exc.message}, code=exc.code)
            elif exc.code == 'not_consistent_weight':
                exc = NotConsistentWeight(detail={'error': exc.message}, code=exc.code)

    response = exception_handler(exc, context)

    return response
