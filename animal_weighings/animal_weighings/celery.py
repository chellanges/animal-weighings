import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'animal_weighings.settings')
app = Celery('animal_weighings')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()
