from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

from rest_framework.schemas import get_schema_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/cattles/', include('cattles.urls')),
    path('api/v1/ranches/', include('ranches.urls')),
    path('api/v1/weighings/', include('weighings.urls')),
    path('api/v1/reports/', include('reports.urls')),
    path('api/v1/tasks/', include('tasks.urls')),
    path('api/v1/openapi/', get_schema_view(
            title="Animal Weighings",
            description="Standard animal weighing register.",
            version="1.0.0"
        ), name='openapi-schema'),
    path('api/v1/docs/', TemplateView.as_view(
            template_name='swagger-ui.html',
            extra_context={'schema_url': 'openapi-schema'}
        ), name='swagger-ui'),
]
