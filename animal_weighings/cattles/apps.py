from django.apps import AppConfig


class CattlesConfig(AppConfig):
    name = 'cattles'

    def ready(self):
        # noinspection PyUnresolvedReferences
        import cattles.signals  # noqa
