from rest_framework.exceptions import ValidationError


class DuplicatedCattle(ValidationError):
    status_code = 409
    default_detail = 'There is already cattle on the same ranch and with the same active ear_tag'
    default_code = 'duplicated_cattle'
