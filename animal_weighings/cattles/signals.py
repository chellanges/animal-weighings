from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import Cattle


@receiver(pre_save, sender=Cattle)
def validate_traceability(sender, **kwargs):
    instance = kwargs['instance']
    if instance.is_active:
        cattle = Cattle.objects.filter(ear_tag=instance.ear_tag,
                                       ranch=instance.ranch,
                                       is_active=True)
        if instance.id:
            cattle = cattle.exclude(id=instance.id)
        if cattle.exists():
            raise ValidationError('There is already cattle on the same ranch '
                                  'and with the same active ear_tag', code='duplicated_cattle')
