from django.db import models

from ranches.models import Ranch


class Cattle(models.Model):
    TYPES_CHOICES = [
        ('cow', 'Cow'),
        ('ox', 'Ox'),
    ]
    type = models.CharField(max_length=3, choices=TYPES_CHOICES)
    ear_tag = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    ranch = models.ForeignKey(
        Ranch,
        related_name='cattle',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return '({}) {}'.format(self.is_active, self.ear_tag)
