from rest_framework import viewsets

from .models import Cattle
from .serializers import CattleSerializer


class CattleViewSet(viewsets.ModelViewSet):
    queryset = Cattle.objects.all()
    serializer_class = CattleSerializer
