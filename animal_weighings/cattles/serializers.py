from rest_framework import serializers

from .models import Cattle, Ranch


class CattleSerializer(serializers.ModelSerializer):
    ranch__id = serializers.PrimaryKeyRelatedField(
        source='ranch', queryset=Ranch.objects.all())
    ranch__name = serializers.ReadOnlyField(source='ranch.name')

    class Meta:
        model = Cattle
        fields = ['type', 'ear_tag', 'is_active', 'ranch__id', 'ranch__name']
