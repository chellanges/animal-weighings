import json

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status

from cattles.models import Cattle
from ranches.models import Ranch
from cattles.serializers import CattleSerializer


class GetAllCattleTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)

        Cattle.objects.create(type='cow', ear_tag='sjhe645bk!@?kuw4678!', ranch=new_ranch)
        Cattle.objects.create(type='cow', ear_tag='.dsfklhgp984570)(&*', ranch=new_ranch)
        Cattle.objects.create(type='ox', ear_tag='fsdhglkh12345)*&', ranch=new_ranch)
        Cattle.objects.create(type='ox', ear_tag='ljkdhg918246*&', ranch=new_ranch)

    def test_get_all_cattle(self):
        """
        GIVEN a Django application
        WHEN the '/api/v1/cattles' page is requested (GET)
        THEN check the response is valid
        """
        # get API response
        response = self.client.get(reverse('cattles-list'))
        # get data from db
        cattle = Cattle.objects.all()
        serializer = CattleSerializer(cattle, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleCattleTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)

        self.cattle_first = Cattle.objects.create(
            type='cow', ear_tag='sjhe645bk!@?kuw4678!', ranch=new_ranch)
        self.cattle_second = Cattle.objects.create(
            type='cow', ear_tag='.dsfklhgp984570)(&*', ranch=new_ranch)
        self.cattle_third = Cattle.objects.create(
            type='ox', ear_tag='fsdhglkh12345)*&', ranch=new_ranch)
        self.cattle_fourth = Cattle.objects.create(
            type='ox', ear_tag='ljkdhg918246*&', ranch=new_ranch)

    def test_get_valid_single_cattle(self):
        response = self.client.get(
            reverse('cattles-detail', kwargs={'pk': self.cattle_first.pk}))
        cattle = Cattle.objects.get(pk=self.cattle_first.pk)
        serializer = CattleSerializer(cattle)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_cattle(self):
        response = self.client.get(
            reverse('cattles-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewCattleTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        Ranch.objects.create(name='Test Ranch', user=user)
        self.valid_payload = {
            'type': 'cow',
            'ear_tag': 'sjhe645bk!@?kuw4678!',
            'ranch__id': 1,
        }

        self.invalid_payload = {
            'type': 'cow',
            'ear_tag': 'sjhe645bk!@?kuw4678!',
            'ranch__id': 30,
        }

    def test_create_valid_cattle(self):
        response = self.client.post(
            reverse('cattles-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_cattle(self):
        response = self.client.post(
            reverse('cattles-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_duplicated_cattle(self):
        response = self.client.post(
            reverse('cattles-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        response_duplicated = self.client.post(
            reverse('cattles-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_duplicated.status_code, status.HTTP_409_CONFLICT)


class UpdateSingleCattleTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)
        self.cattle_first = Cattle.objects.create(
            type='cow', ear_tag='sjhe645bk!@?kuw4678!', ranch=new_ranch)
        self.cattle_second = Cattle.objects.create(
            type='cow', ear_tag='.dsfklhgp984570)(&*', ranch=new_ranch)
        self.valid_payload = {
            'type': 'ox',
            'ear_tag': 'sjhe645bk!@?kuw4678!',
            'ranch__id': 1,
        }

        self.invalid_payload = [
            {
                'type': 'cow',
                'ear_tag': '.dsfp984570)(&*',
                'ranch__id': 30,
            },
            {
                'type': 'ox',
                'ear_tag': 'sjhe645bk!@?kuw4678!',
                'ranch__id': 1,
            }
        ]

    def test_valid_update_cattle(self):
        response = self.client.put(
            reverse('cattles-detail', kwargs={'pk': self.cattle_first.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_cattle(self):
        response_first = self.client.put(
            reverse('cattles-detail', kwargs={'pk': self.cattle_second.pk}),
            data=json.dumps(self.invalid_payload[0]),
            content_type='application/json')
        response_second = self.client.put(
            reverse('cattles-detail', kwargs={'pk': self.cattle_second.pk}),
            data=json.dumps(self.invalid_payload[1]),
            content_type='application/json')
        self.assertEqual(response_first.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_second.status_code, status.HTTP_409_CONFLICT)


class DeleteSingleCattleTest(TestCase):
    def setUp(self):
        user = get_user_model().objects.create(email='user@email.com')
        user.set_password('12345')
        user.save()

        self.client = Client()
        self.client.login(email='user@email.com', password='12345')

        new_ranch = Ranch.objects.create(name='Test Ranch', user=user)
        self.cattle_first = Cattle.objects.create(
            type='cow', ear_tag='sjhe645bk!@?kuw4678!', ranch=new_ranch)
        self.cattle_second = Cattle.objects.create(
            type='ox', ear_tag='.dsfklhgp984570)(&*', ranch=new_ranch)

    def test_valid_delete_cattle(self):
        response = self.client.delete(
            reverse('cattles-detail', kwargs={'pk': self.cattle_first.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_cattle(self):
        response = self.client.delete(
            reverse('cattles-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
