from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.test import TestCase

from cattles.models import Cattle
from ranches.models import Ranch


class CattleFunctionalTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        self.new_user = MyUser.objects.create_user(email='user@email.com')
        self.new_ranch = Ranch.objects.create(name='Test Ranch', user=self.new_user)
        self.new_cattle = Cattle.objects.create(type='cow',
                                                ear_tag='sjhe645bk!@?kuw4678!',
                                                ranch=self.new_ranch)

    def test_new_cattle(self):
        """
        GIVEN a Cattle model
        WHEN a new Cattle is created
        THEN check the type, ear_tag and ranch are defined correctly
        """
        self.assertTrue(self.new_cattle.is_active)
        self.assertEqual(self.new_cattle.type, 'cow')
        self.assertEqual(self.new_cattle.ear_tag, 'sjhe645bk!@?kuw4678!')
        self.assertEqual(self.new_cattle.ranch.name, 'Test Ranch')
        self.assertIsInstance(self.new_cattle.ranch, Ranch)

    def test_cattle_exists_in_database(self):
        """
        GIVEN a Cattle model
        WHEN a new Cattle is created
        THEN check if exists in database
        """
        existing_cattle = Cattle.objects.filter(ear_tag='sjhe645bk!@?kuw4678!',
                                                is_active=True)
        self.assertTrue(existing_cattle.exists())

    def test_validate_traceability(self):
        """
        GIVEN a Cattle model
        WHEN a new Cattle is created
        THEN check if exists already cattle on the same ranch and active ear_tag
        """
        with self.assertRaises(ValidationError):
            Cattle.objects.create(type='ox',
                                  ear_tag=self.new_cattle.ear_tag,
                                  ranch=self.new_ranch)
