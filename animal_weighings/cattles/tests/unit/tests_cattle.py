from django.contrib.auth import get_user_model
from django.test import TestCase

from cattles.models import Cattle
from ranches.models import Ranch


class CattleUnitTestCase(TestCase):
    def setUp(self):
        MyUser = get_user_model()
        new_user = MyUser(email='test@email.com')
        new_ranch = Ranch(name='Test Ranch', user=new_user)
        self.new_cattle = Cattle(type='cow',
                                 ear_tag='sjhe645bk!@?kuw4678!',
                                 ranch=new_ranch)

    def test_new_cattle(self):
        """
        GIVEN a Cattle model
        WHEN a new Cattle is created
        THEN check the type, ear_tag and ranch are defined correctly
        """
        self.assertEqual(self.new_cattle.type, 'cow')
        self.assertEqual(self.new_cattle.ear_tag, 'sjhe645bk!@?kuw4678!')
        self.assertEqual(self.new_cattle.ranch.name, 'Test Ranch')
        self.assertIsInstance(self.new_cattle.ranch, Ranch)

    def test_new_cattle_default_is_active(self):
        """
        GIVEN a Cattle model
        WHEN a new Cattle is created
        THEN check the default is_active is defined correctly
        """
        self.assertTrue(self.new_cattle.is_active)
