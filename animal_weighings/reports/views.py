from rest_framework import viewsets

from weighings.models import Weighing
from weighings.filtersets import WeighingsFilter

from .serializers import WeighingsReportSerializer


class WeighingsReportViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = WeighingsReportSerializer
    filterset_class = WeighingsFilter

    def get_queryset(self):
        weighings = Weighing.objects.select_related('cattle').filter(
            cattle__ranch__user=self.request.user
        ).order_by('date_time')

        return weighings
