from rest_framework import serializers

from weighings.models import Weighing


class WeighingsReportSerializer(serializers.ModelSerializer):
    cattle__id = serializers.ReadOnlyField(source='cattle_id')
    cattle__type = serializers.ReadOnlyField(source='cattle.type')
    cattle__ear_tag = serializers.ReadOnlyField(source='cattle.ear_tag')
    cattle__is_active = serializers.ReadOnlyField(source='cattle.is_active')
    cattle__ranch__name = serializers.ReadOnlyField(source='cattle.ranch.name')

    class Meta:
        model = Weighing
        fields = ['date_time', 'weight', 'weight_gain', 'cattle__id', 'cattle__type',
                  'cattle__ear_tag', 'cattle__is_active', 'cattle__ranch__name', ]
