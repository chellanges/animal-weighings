from django.urls import include, path
from rest_framework import routers

from .views import WeighingsReportViewSet

router = routers.DefaultRouter()
router.register(r'weighings', WeighingsReportViewSet, basename='reports-weighings')

urlpatterns = [
    path('', include(router.urls)),
]
