# Desafio Jetbov - Backend

## Considerações Gerais

Você deverá usar um repositório para desenvolver seu projeto, todos os seus commits devem estar
registrados, pois queremos ver como você trabalha;

Sua solução deve ser simples de ser executada, seguindo as condições abaixo:
* Precisamos conseguir rodar seu código no Mac e Linux;
* Registre tudo: escolhas feitas para a solução, testes que foram executados, outras ideias
que gostaria de implementar se tivesse mais tempo (explique como você as resolveria, se
houvesse tempo), decisões que foram tomadas e os seus porquês, arquiteturas que foram
testadas e os motivos de terem sido modificadas ou abandonadas;
* Documente as dependências necessárias, instruções de compilação, instalação, e execução.
Por exemplo:  
```
git clone .../repositorio.git
cd repositorio
./configure
make```
```

## O Problema a ser resolvido

Você deve desenvolver uma aplicação backend (API), com uma interface de administração, para
cadastro de pesagens de animais, no padrão REST, com Python.

##### As entidades que devem ser tratadas na api são:

* Usuário - Usuarios da aplicação fazem login com email
* Fazenda - Cada usuário pode ter mais de uma fazenda cadastrada
* Animal - Boi ou Vaca
* Pesagem - Periodicamente os animais são pesados na fazenda

##### O que a api necessita ter:

* CRUD de animais/fazendas/pesagens via API;
* Administração dos dados de animais/fazendas/pesagens via interface de administração;
* Todo animal dentro da fazenda é identificado por um "brinco", que é um campo
alfanumérico;
* Um endpoint para geração de dados para um relatório sobre as pesagens, com informacoes
do animal, suas datas de pesagem, informações dos pesos e ganho de peso (que é o a
diferença entre o peso atual e o peso anterior), com filtro para período, "brinco" do animal
ou fazenda, todos opcionais, e restringindo os dados somente aos que o usuário informado
na autenticação da api tenha acesso;
* A criação dos animais precisa validar se não existem animais na mesma fazenda com o
mesmo brinco, e que estejam ativos, se não, perdemos a rastreabilidade;
* Ao salvar um novo peso, a aplicação deve validar se está coerente, ou seja: não zero e
maiores que 80% do peso informado anteriormente caso houver, além de que devem ser
salvos de forma assíncrona;
* Documentação da API para apresentar à outros desenvolvedores que teoricamente irão
utilizar para fazer integrações;
* Testes unitários e de integração
* Utilizar banco de dados relacional

## O que será avaliado na sua solução?

Seu código será observado por uma equipe de desenvolvedores que avaliará a simplicidade e
clareza da solução, a arquitetura, documentação, estilo de código, testes unitários, testes
funcionais, nível de automação dos testes, implementação do código, e a organização geral
projeto.

O cuidado com boas práticas de programação, de utilização do git, conhecimento sobre Python
serão avaliados;

Seja legal com a gente e gere dados de exemplo para testarmos ;-)

Fique a vontade para escolher seu setup para desenvolver e não esqueça de detalhar todas as
escolhas.

Caso você tenha alguma dúvida sobre as informações solicitadas você pode enviar um email
para sergio@jetbov.com que responderei sem problemas. Não fique com dúvidas, comunicação
também é importante!
