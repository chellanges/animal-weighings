# Jetbov - Animal Weighings

The test specification is described [here](SPECIFICATION.md).

## Description

Backend application (API), with an administration interface, to REST standard animal weighing register with Python.

## Usage

To run this app you'll need:

* Python
* PIPENV
* docker
* docker-compose

There is one easy ways to run and test this app.

### Locally

1. Clone this repository.
2. Create a .env file. Use `sample.env` as a example.
2. Run: `docker-compose up -d --build`

Notes:
* Four services will be created with the following container names: redis-weighings,
postgres-weighings, app-weighings and worker-weighings.  
They will be run in daemon mode and logs can be parsed using the following command: `docker-compose logs <container-name>`
* API documentation can be verified on the route: `http://localhost:8000/api/v1/docs`
* Background tasks can be checked on the route: `http://localhost:8000/api/v1/tasks`
* Admin panel access: `http://localhost:8000/admin`
* Authentication is via Basic Authentication and Session Authentication,  
and to create a new user, run: `docker exec -it app-weighings python manage.py createsuperuser`

## Testing

Before running the tests, the dependencies need to be installed.  

Required Steps:
```
$ pipenv install --dev
```

To test, just run:
```
$ cd animal_weighings
$ python manage.py test
```

To tox, just run:
```
$ tox
```
Notes:
* Before running the tests, be sure to set the environment variable to set the database locally.
* In the `.env` file, just comment or remove the line containing the `DATABASE_URL` variable and
the default settings will be set. This will create a sqlite database locally.

## Implementation Details

About the development environment, I'm developing this project using a Notebook,
running openSUSE Tumbleweed. I have written this text and code entirely in PyCharm.

To develop this app I added some extra dependencies:

* *[Django](https://github.com/django/django):* Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design
* *[djangorestframework](https://github.com/encode/django-rest-framework):* Django REST framework is a powerful and flexible toolkit for building Web APIs.
* *[prettyconf](https://github.com/osantana/prettyconf):* Pretty Conf is a Python library created to make easy the separation of configuration and code following the recomendations of 12 Factor's topic about configs.
* *[dj-database-url](https://github.com/kennethreitz/dj-database-url):* This simple Django utility allows you to utilize the 12factor inspired DATABASE_URL environment variable to configure your Django application.
* *[Markdown](https://pypi.org/project/Markdown/):* Markdown support for the browsable API.
* *[django-filter](https://github.com/carltongibson/django-filter):* A generic system for filtering Django QuerySets based on user selections.
* *[coverage](https://github.com/nedbat/coveragepy):* Code coverage measurement for Python.
* *[celery](http://www.celeryproject.org/):* Is an asynchronous task queue/job queue based on distributed message passing.
* *[django-celery-results](https://pypi.org/project/django-celery-results/):* Celery result backends for Django.
* *[redis](https://redis.io/):* is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker.
* *[pyyaml](https://pypi.org/project/PyYAML/):* YAML parser and emitter for Python
* *[uritemplate](https://pypi.org/project/uritemplate/):* URI templates.
* *[gunicorn](https://github.com/benoitc/gunicorn):* WSGI HTTP Server for UNIX, fast clients and sleepy applications.
* *[psycopg2](https://github.com/psycopg/psycopg2):* PostgreSQL database adapter for the Python programming language.
* *[tox](https://tox.readthedocs.io/en/latest/):* Tox aims to automate and standardize testing in Python.
